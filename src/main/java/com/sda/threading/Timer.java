package com.sda.threading;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Timer {
//    Wykorzystując wątki i klasy stwórz Projekt "timer".
//    Założeniem aplikacji jest bycie timerem.
//    Aplikacja ma dwie komendy:
//    start N
//    stop N
//    listrunning
//    Założenia:
//      - mozemy wylistowac statusy timerow komenda listrunning
//      - startując timer podajemy N - numer timera
//      - mierzymy czas i podajemy: czas startu, czas stopu, czas w milisekundach.
//      - oprzyj rozwiązanie o wątki.

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;

        List<TimerRunnable> timers = new ArrayList<>();
        ExecutorService watki = Executors.newFixedThreadPool(10);
        while (isWorking) {
            String line = sc.nextLine();

            if (line.startsWith("start")) {
                line = line.replace("start ", "");
                Long time = Long.parseLong(line);

                TimerRunnable runnable = new TimerRunnable(time);
                watki.submit(runnable);

                timers.add(runnable);
            } else if (line.equals("status")) {
                TimerRunnable[] tmp = new TimerRunnable[timers.size()];
                tmp = timers.toArray(tmp);

                for (TimerRunnable r : tmp) {
                    if (r.hasFinished()) {
                        timers.remove(r);
                    }
                    r.print();
                }
            }
        }
    }
}
