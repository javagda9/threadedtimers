package com.sda.threading;

public class TimerRunnable implements Runnable {
    private long time;
    private long timeStart = 0L;
    private long current;

    private boolean hasFinished = false;
    public TimerRunnable(long time) {
        this.time = time;
    }

    public void run() {
        timeStart = System.currentTimeMillis();
        current = System.currentTimeMillis();

        while ((current - timeStart) < time) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            current = System.currentTimeMillis();
        }
        System.out.println("Timer has finished: " + time);
        hasFinished = true;
    }

    public void print() {
        System.out.println(" Timer: " + time + " - Zostalo mi " + (time - (System.currentTimeMillis() - timeStart)));
    }

    public boolean hasFinished() {
        return hasFinished;
    }
}
